<?php

namespace Drupal\uw_cbl_facebook\EventSubscriber;

use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormBaseAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_cfg_common\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UwCblFacebookPreprocessBlock.
 */
class UwCblFacebookPreprocessBlock extends UwCblBase implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

  /**
   * Preprocess blocks with facebook and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_facebook')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block
      $block = $variables->getByReference('content')['#block_content'];

      // Get the feed type.
      $feed_type = $block->get('field_uw_fb_feed_type')->getValue()[0]['value'];

      // Get the settings for facebook.
      $facebook = [
        'feed_type' => $feed_type,
        'username' => ($feed_type == 'timeline') ? $block->get('field_uw_fb_page')->getValue()[0]['value'] : '',
        'url' => ($feed_type == 'singlepost') ? $block->get('field_uw_fb_url')->getValue()[0]['value'] : '',
      ];

      // Set the settings for facebook in variables.
      $variables->set('facebook', $facebook);
    }
  }
}
