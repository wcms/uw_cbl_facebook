<?php

namespace Drupal\uw_cbl_facebook\EventSubscriber;

use Drupal\Core\Form\FormStateInterface;
use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormBaseAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;
use Drupal\uw_cfg_common\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UwCblFacebookAlterForm.
 */
class UwCblFacebookAlterForm extends UwCblBase implements EventSubscriberInterface {

  /**
   * Alter form.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormAlterEvent $event
   *   The event.
   */
  public function alterForm(FormAlterEvent $event): void {

    // Check that we are on a layout builder add/update and on Facebook block.
    if ($this->checkLayoutBuilder($event, 'Facebook')) {

      // Get the form from the event.
      $form = &$event->getForm();

      // Add the validation for facebook.
      $form['#validate'][] = [$this, 'uw_cbl_facebook_validation'];

      // Add the custom form alter to the layout builder.
      $form['settings']['block_form']['#process'][] = [$this, 'uw_cbl_facebook_process_facebook_type'];
    }
  }

  /**
   * Form validation for facebook.
   *
   * @param array $form
   *   The complete form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function uw_cbl_facebook_validation(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {

    // Get the settings from the form.
    $settings = $form_state->getValue('settings', NULL);

    // If there are settings, continue to process.
    if ($settings) {

      // If there is a block, continue to process.
      if ($block = $settings['block_form']) {

        // If there is a type of facebook feed, continue to process.
        if ($type = $block['field_uw_fb_feed_type'][0]['value']) {

          // If the type is timeline, validate page.
          if ($type == 'timeline') {

            // If there is a facebook page value, check it, if not set error about blank.
            if (isset($block['field_uw_fb_page'][0]['value']) && $block['field_uw_fb_page'][0]['value'] !== "") {

              // Get the page form value.
              $page = $block['field_uw_fb_page'][0]['value'];

              // Check if we do not have a valid facebook page.
              if (!preg_match('/[a-zA-Z]/', $page) || !preg_match('/^(?:[-.a-zA-Z0-9]{1,75}|pages\/[-.a-zA-Z0-9]+\/[0-9]+)$/', $page)) {

                // Set the form error.
                $form_state->setErrorByName('settings][block_form][field_uw_fb_page', 'The FaceBook username contains errors.');
              }
            }
            else {

              // Set the form error.
              $form_state->setErrorByName('settings][block_form][field_uw_fb_page', 'You must enter a FaceBook page.');
            }
          }

          // If we are on a single post, continue to process it.
          if ($type == "singlepost") {

            // If there is a facebook url value, check it, if not set error about blank.
            if (isset($block['field_uw_fb_url'][0]['value']) && $block['field_uw_fb_url'][0]['value'] !== "") {

              // Get the url form value.
              $url = $block['field_uw_fb_url'][0]['value'];

              // Check if we do not have a valid facebook page.
              if (!preg_match('/^https?:\/\/(?:www\.)?facebook\.com\/.+/', $url)) {

                // Set the form error.
                $form_state->setErrorByName('settings][block_form][field_uw_fb_url', 'The FaceBook url contains errors.');
              }
            }
            else {

              // Set the form error.
              $form_state->setErrorByName('settings][block_form][field_uw_fb_url', 'You must enter a FaceBook url.');
            }
          }
        }
      }
    }
  }

  /**
   * Get the layout object
   *
   * @param FormStateInterface $form_state
   *   The form state.
   * @return mixed
   *   The element.
   */
  public function uw_cbl_facebook_process_facebook_type(array $element, \Drupal\Core\Form\FormStateInterface $form_state) {

    // Fields to check for.
    $fields = [
      'field_uw_fb_page' => [
        'values' => [
          ['value' => 'timeline'],
        ]
      ],
      'field_uw_fb_url' => [
        'values' => [
          ['value' => 'singlepost'],
        ]
      ],
    ];

    // Step through each field and add the states if they are on the form.
    foreach ($fields as $field => $values) {

      // If the field is in the form add the states.
      if (in_array($field, $element)) {

        // The selector that will determine if visible.
        $selector = 'select[name="settings[block_form][field_uw_fb_feed_type]"]';

        $element[$field]['#states'] = [
          'visible' => [
            $selector => [
              $values['values'],
            ]
          ],
        ];
      }
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      HookEventDispatcherInterface::FORM_ALTER => 'alterForm',
    ];
  }
}
